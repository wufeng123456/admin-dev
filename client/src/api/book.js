import request from '@/utils/request'

export function addBook(data) {
  return request({
    url: '/book/add',
    method: 'post',
    data
  })
}

export function updateBook(data) {
  return request({
    url: '/book/update',
    method: 'post',
    data
  })
}

// export function updateBook(data) {
//   return request({
//     url: '/book/update',
//     method: 'post',
//     data
//   })
// }

export function getBooks() {
  return request({
    url: '/book/list',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
