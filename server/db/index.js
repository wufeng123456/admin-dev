const mysql = require('mysql')
const config = require('./config')

function connect() {
  return mysql.createConnection(config)
}

function querySql (sql) {
  const conn = connect()
  return new Promise((resolve, reject) => {
    try {
      conn.query(sql, (err, result) => {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      })
    } catch (error) {
      reject(error)
    } finally {
      conn.end()
    }
  })
}

function queryOne (sql) {
  return new Promise((resolve, reject) => {
    querySql(sql).then(results => {
      if (results && results.length >= 0) {
        resolve(results[0])
      } else {
        resolve(null)
      }
    }).catch(err => {
      reject(err)
    })
  })
}

function addSql (sql) {
  const conn = connect()
  return new Promise((resolve, reject) => {
    try {
      conn.query(sql, (err, result) => {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      })
    } catch (error) {
      reject(error)
    } finally {
      conn.end()
    }
  })
}

module.exports = {
  querySql,
  queryOne,
  addSql
}