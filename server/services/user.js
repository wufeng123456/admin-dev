const { querySql, queryOne, addSql } = require('../db/index')

function login (username, password) {
  return querySql(`select * from admin_user where username = '${username}' and password = '${password}'`)
}

function findUser (username) {
  return queryOne(`select * from admin_user where username = '${username}'`)
}

function findUsers (conditions) {
  console.log(conditions)
  if (conditions.searchValue) {
    return querySql(`select * from admin_user where username like '%${conditions.searchValue}%' limit ${(conditions.pageIndex - 1) * conditions.pageSize}, ${conditions.pageSize}`)
  } else {
    return querySql(`select * from admin_user limit ${(conditions.pageIndex - 1) * conditions.pageSize}, ${conditions.pageSize}`)
  }
}

function addUser (user) {
  return addSql(`insert into admin_user (username, role, password, nickname) values ('${user.username}', '${user.role}', '${user.password}', '${user.nickname}')`)
}

function updateUser (user) {
  return addSql(`update admin_user set role = '${user.role}', nickname='${user.nickname}' where id = '${user.id}'`)
}

function deleteUser (id) {
  return addSql(`delete from admin_user where id = '${id}'`)
}

function getTotal () {
  return querySql(`select count(*) as total from admin_user`)
}

module.exports = {
  login,
  findUser,
  findUsers,
  addUser,
  getTotal,
  updateUser,
  deleteUser
}