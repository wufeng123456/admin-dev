const express = require('express')
const router = require('./router')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/', router)

const server = app.listen(5000, () => {
  const { address, port } = server.address()
  console.log('http server is running at http://localhost:%s', port)
})

