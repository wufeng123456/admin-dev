const express = require('express')

const router = express.Router()

const { md5, decoded } = require('../utils')
const jwt = require('jsonwebtoken')

const { PWD_SALT, PRIVATE_KEY, JWT_EXPRIED } = require('../utils/constant')

const Result = require('../models/Result')

const { login, findUser, findUsers, addUser, getTotal, updateUser, deleteUser } = require('../services/user')

router.get('/info', (req, res) => {
  const decode = decoded(req)
  if (decode && decode.username) {
    findUser(decode.username).then(user => {
      if (user) {
        user.roles = [user.role]
        new Result(user, '用户信息查询成功').success(res)
      } else {
        new Result('用户信息查询失败').fail(res)
      }
    })
  } else {
    new Result('用户信息查询失败').fail(res)
  }
})

router.post('/list', (req, res) => {
  const conditions = { ...req.body }
  console.log(conditions)
  findUsers(conditions).then(users => {
    getTotal().then(total => {
      const page = {
        total: total[0].total
      }
      new Result(users, '用户信息查询成功', {
        page: page
      }).success(res)
    })
  })
})

router.post('/add', (req, res) => {
  const user = { ...req.body }
  user.password = md5(`123456${PWD_SALT}`)
  addUser(user).then(result => {
    new Result(result, '用户信息新增成功').success(res)
  })
})

router.patch('/update/:id', (req, res) => {
  const id = req.params.id
  const user = {
    id: id,
    ...req.body
  }
  updateUser(user).then(result => {
    new Result(result, '用户信息更新成功').success(res)
  })
})

router.delete('/delete/:id', (req, res) => {
  const id = req.params.id
  deleteUser(id).then(result => {
    new Result(result, '用户信息删除成功').success(res)
  })
})

router.post('/login', (req, res) => {
  let { username, password } = req.body
  password = md5(`${password}${PWD_SALT}`)
  console.log(password)
  login(username, password).then(result => {
    console.log(result)
    if (!result || result.length === 0) {
      new Result('账户或密码错误').success(res)
    } else {
      const token = jwt.sign(
        { username },
        PRIVATE_KEY,
        { expiresIn: JWT_EXPRIED }
      )
      new Result({ token }, '登陆成功').success(res)
    }
  })
})

module.exports = router