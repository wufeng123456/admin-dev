const express = require('express')
const router = express.Router()
const userRouter = require('./user')
const boom = require('boom')
const jwtAuth = require('../utils/jwt')
const Result = require('../models/Result')
router.get('/', (req, res) => {
  res.send('欢迎学习狗哥后台管理系统')
})

router.use(jwtAuth)
router.use('/user', userRouter)

router.use((req, res, next) => {
  next(boom.notFound('接口不存在'))
})

router.use((err, req, res, next) => {
  const { status = 401, message } = err
  if (err.name && err.name === 'UnauthorizedError') {
    new Result(null, 'Token验证失败', {
      error: status,
      errorMsg: message
    }).tokenError(res.status(status))
  } else {
    const msg = (err && err.message) || '系统错误'
    const statusCode = (err.output && err.output.code) || 500
    const errorMsg = (err.output && err.output.payload && err.output.payload.error) || err.message
    new Result(null, msg, {
      error: statusCode,
      errorMsg
    }).fail(res.status(statusCode))
  }
})

module.exports = router