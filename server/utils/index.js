
const crypto = require('crypto')
const { PRIVATE_KEY } = require('./constant')
const jwt = require('jsonwebtoken')

function md5 (s) {
  return crypto
    .createHash('md5')
    .update(String(s))
    .digest('hex')
}

function decoded (req) {
  const authorization = req.get('Authorization')
  if (authorization.indexOf('Bearer ') === 0) {
    token = authorization.replace('Bearer ', '')
  }
  return jwt.verify(token, PRIVATE_KEY)
}

module.exports = {
  md5,
  decoded
}